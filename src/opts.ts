import { schema } from "./schema";
import { prisma } from "./generated/prisma-client";
import { getUserFromContext } from "./extensions/authentication/utils";
import { Context } from "./types/context";

export default {
    schema,

    context: async (ctx: Context) => {
        const user = await getUserFromContext(prisma, ctx);

        return {
            ...ctx,
            user,
            prisma
        };
    }
};
