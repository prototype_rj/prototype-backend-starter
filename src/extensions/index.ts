import authentication from "./authentication";
import email from "./email";

export default [...authentication, ...email];
