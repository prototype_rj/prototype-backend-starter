import { verify } from "jsonwebtoken";

import { Context } from "../../types/context";
import { User, Prisma } from "../../generated/prisma-client";

interface Token {
    userId: string;
}

export function getUserId(context: Context) {
    const Authorization = context.request.get("Authorization");
    if (Authorization) {
        const token = Authorization.replace("Bearer ", "");
        try {
            const verifiedToken = verify(token, process.env.APP_SECRET) as Token;
            return verifiedToken && verifiedToken.userId;
        } catch (e) {
            return null;
        }
    }
}

export async function getUserFromContext(prisma: Prisma, context: Context): Promise<User | null> {
    const userId = getUserId(context);
    if (userId) {
        try {
            return prisma.user({ id: userId });
        } catch (e) {
            return null;
        }
    }

    return null;
}
