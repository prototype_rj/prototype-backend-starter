import { extendType } from "nexus/dist";

export const AuthenticationQueries = extendType({
    type: "Query",
    definition(t) {
        t.field("currentUser", {
            type: "User",
            resolve: (parent, args, ctx) => {
                return ctx.user;
            }
        });
    }
});
