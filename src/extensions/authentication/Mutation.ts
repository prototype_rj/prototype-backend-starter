import { extendType, arg } from "nexus";
import { hash, compare } from "bcryptjs";
import { sign } from "jsonwebtoken";
import * as cuid from "cuid";

import { AuthPayload } from "./AuthPayload";

export const SignupLoginMutations = extendType({
    type: "Mutation",
    definition(t) {
        t.field("forgotPassword", {
            type: "Boolean",
            args: {
                data: arg({
                    type: "UserWhereUniqueInput",
                    required: true
                })
            },
            async resolve(root, { data: where }, ctx, info) {
                const user = await ctx.prisma.user(where);
                if (user) {
                    await ctx.prisma.updateUser({
                        where,
                        data: {
                            forgotPasswordToken: cuid()
                        }
                    });

                    return true;
                } else {
                    return false;
                }
            }
        });

        t.field("resetPassword", {
            type: "Boolean",
            args: {
                forgotPasswordToken: arg({
                    type: "String",
                    required: true
                }),
                newPassword: arg({
                    type: "String",
                    required: true
                })
            },
            async resolve(root, { forgotPasswordToken, newPassword }, ctx, info) {
                const user = await ctx.prisma.user({
                    forgotPasswordToken
                });
                if (user) {
                    // now reset the password to the user specified password
                    const hashedPassword = await hash(newPassword, 10);
                    await ctx.prisma.updateUser({
                        where: {
                            forgotPasswordToken
                        },
                        data: {
                            password: hashedPassword,
                            forgotPasswordToken: null
                        }
                    });
                    return true;
                } else {
                    throw new Error(`Error while resetting password for ${user}`);
                }
            }
        });

        t.field("signup", {
            type: AuthPayload,
            args: {
                data: arg({
                    type: "UserCreateInput",
                    required: true
                })
            },
            async resolve(root, { data }, ctx, info) {
                const hashedPassword = await hash(data.password, 10);

                // TODO: This type breaks
                const user = await ctx.prisma.createUser({
                    ...data,
                    password: hashedPassword
                } as any);

                return {
                    token: sign({ userId: user.id }, process.env.APP_SECRET),
                    user
                };
            }
        });

        t.field("login", {
            type: AuthPayload,
            args: {
                data: arg({
                    type: "UserCreateInput",
                    required: true
                })
            },
            resolve: async (parent, { data: { email, password } }, context) => {
                const user = await context.prisma.user({ email });
                if (!user) {
                    throw new Error(`No user found for email: ${email}`);
                }
                const passwordValid = await compare(password, user.password);
                if (!passwordValid) {
                    throw new Error("Invalid password");
                }

                if (user.forgotPasswordToken != null) {
                    throw new Error("User must reset their password");
                }

                return {
                    token: sign({ userId: user.id }, process.env.APP_SECRET),
                    user
                };
            }
        });
    }
});
