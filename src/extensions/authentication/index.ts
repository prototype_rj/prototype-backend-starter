import { AuthPayload } from "./AuthPayload";
import { SignupLoginMutations } from "./Mutation";
import { AuthenticationQueries } from "./Query";

export default [AuthPayload, SignupLoginMutations, AuthenticationQueries];
