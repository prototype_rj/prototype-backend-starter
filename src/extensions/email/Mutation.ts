import { extendType, arg, inputObjectType } from "nexus/dist";
import * as sgMail from "@sendgrid/mail";

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

export const EmailInput = inputObjectType({
    name: "EmailInput",
    definition(t) {
        t.string("to", {
            nullable: false
        });

        t.string("from", {
            nullable: false
        });

        t.string("subject", {
            nullable: false
        });

        t.string("html");
    }
});

export const EmailMutation = extendType({
    type: "Mutation",
    definition(t) {
        t.field("email", {
            type: "Boolean",
            args: {
                data: arg({
                    type: "EmailInput",
                    required: true
                })
            },
            async resolve(root, { data }, ctx, info) {
                try {
                    await sgMail.send(data);
                    return true;
                } catch (e) {
                    console.error(e);
                    return false;
                }
            }
        });
    }
});
