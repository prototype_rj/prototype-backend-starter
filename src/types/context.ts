import { Request } from "express";

import { Prisma, User } from "../generated/prisma-client";

export interface Context {
    request: Request;
    prisma: Prisma;
    user: User | null;
}
