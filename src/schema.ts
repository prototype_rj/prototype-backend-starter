import * as path from "path";
import { makePrismaSchema, prismaObjectType } from "nexus-prisma";

import { prisma } from "./generated/prisma-client";
import datamodelInfo from "./generated/nexus-prisma";
import extensions from "./extensions";

const Query = prismaObjectType({
    name: "Query",
    definition: t => t.prismaFields(["*"])
});

const Mutation = prismaObjectType({
    name: "Mutation",
    definition: t => t.prismaFields(["*"])
});

let typegenIncludeConfig = {};

if (process.env.NODE_ENV !== "production") {
    typegenIncludeConfig = {
        typegenAutoConfig: {
            sources: [
                {
                    source: path.join(__dirname, "./types/context.ts"),
                    alias: "ctx"
                }
            ],
            contextType: "ctx.Context"
        }
    };
}

export const schema = makePrismaSchema({
    types: [Query, Mutation, ...extensions],

    prisma: {
        datamodelInfo,
        client: prisma
    },

    ...typegenIncludeConfig,

    outputs: {
        schema: path.join(__dirname, "./generated/schema.graphql"),
        typegen: path.join(__dirname, "./generated/nexus.ts")
    }
});
