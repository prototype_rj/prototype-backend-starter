import { GraphQLServer } from "graphql-yoga";
import * as companion from "@uppy/companion";
import { Request } from "express";

import opts from "./opts";

const server = new GraphQLServer(opts);

const uppyOptions = {
    providerOptions: {
        s3: {
            key: process.env.AWS_IMAGE_BUCKET_KEY,
            secret: process.env.AWS_IMAGE_BUCKET_SECRET,
            bucket: process.env.AWS_IMAGE_BUCKET,
            region: process.env.AWS_BUCKET_REGION,
            getKey: (req: Request, filename: string) => {
                const def = `${process.env.APP_ID}/${filename}`;

                return req.header("uploadPath") || def;
            }
        }
    },
    filePath: "./output",
    secret: process.env.APP_SECRET,
    debug: process.env.NODE_ENV !== "production"
};

server.use(companion.app(uppyOptions));

server.start(() => console.log(`🚀 Server ready at http://localhost:4000`));
